from scapy.all import AsyncSniffer, sendp, Packet, get_if_hwaddr, get_if_addr
from queue import Queue, Empty
from typing import Callable
import threading
import logging
import time

class tap:

    def __init__(self, iface_name: str, handler: Callable[[Packet], None], verbose=False):
        
        self.handler = handler
        self.iface_name = iface_name

        self.hwaddr = get_if_hwaddr(iface_name)
        self.ipaddr = get_if_addr(iface_name)

        self.alive = True
        self.q = Queue()
        
        self.logger = logging.getLogger(F"{iface_name} TAP")
        self.logger.setLevel(logging.INFO if verbose else logging.ERROR)

        self.logger.info(F"Setting up with MAC: {self.hwaddr} and IP: {self.ipaddr}")
        self.logger.info("Starting AsyncSniffer")
        self.listener = AsyncSniffer(iface=iface_name, prn=lambda p: self.q.put(p))
        self.listener.start()

        self.logger.info("Starting packet handling loop")        
        handling_thread = threading.Thread(target=self._loop)
        handling_thread.start()
    
    def _loop(self):

        while 7:
            if not self.alive:
                return

            try:
                p = self.q.get(block=False)
            except Empty:
                continue

            try:    
                self.handler(self, p)
            except Exception as e:
                self.logger.error(f'Exception in packet handler: {e}')

    def shutdown(self) -> None:
        self.listener.stop()
        self.alive = False

    def sendp(self, pack: Packet) -> None:

        try:
            sendp(pack, iface=self.iface_name, verbose=False)
        except Exception as e:
            self.logger.error(f'Failed to send packet on iface {self.iface_name}: {e}')
