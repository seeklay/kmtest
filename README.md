## About
This python mudule providing functions of classic tun/tap drivers via pcap driver that you have if you using wireshark. To use it you'll need to [create virtual KM-TEST](INSTALLATION.md) adapter in device manager of your Windows system.
> Warning! Although it looks reliable, I won't recommend it as a full alternative for the tun/tap drivers. For tests and experiments - yes, but not for serious use.

#### Requirements

- scapy (for high-level accesing to the driver api)
- pcap driver (for manipulating packets on interface)

#### How to create and configure km-test adapter?
* [CREATING](INSTALLATION.md)
* [CONFIGURING](SETTINGS.md)

#### Installation

* Install pcap driver if you don't have it yet
* Create at least one KM-TEST adapter
* Clone this repo and install it

```bash
git clone https://gitlab.com/seeklay/kmtest.git
pip3 install .
```

* Done

#### Usage example
> This script imitating computer network.
> It responds to ARP and ICMP requests for make visibility of working network.
```python
from scapy.all import *
from kmtest import tap
from time import sleep

def phandler(self, pack):
    if ARP in pack:
        if pack[ARP].op != ARP.op.s2i['who-has']:
            return
        response = Ether(src='FC:FC:48:AE:AE:AE', dst=pack[Ether].src)/\
            ARP(
                op = ARP.op.s2i['is-at'],
                hwsrc = 'FC:FC:48:AE:AE:AE',
                psrc = pack[ARP].pdst,
                hwdst = pack[Ether].src,
                pdst = pack[ARP].psrc
            )
        self.sendp(response)
        print(F"Respond to ARP who-has to addr {pack[ARP].pdst} as FC:FC:48:AE:AE:AE")
        return
  
    if ICMP in pack:
        if pack[ICMP].type != ICMP.type.s2i['echo-request']:
            return
        response = Ether(src='FC:FC:48:ae:ae:ae', dst=pack[Ether].src)/\
        IP(src=pack[IP].dst, dst=pack[IP].src)/\
        ICMP(type=ICMP.type.s2i['echo-reply'], id=pack[ICMP].id, seq=pack[ICMP].seq)/pack.load
  
        self.sendp(response)
        print(F"Respond to ICMP ping to addr {pack[IP].dst}")
        return
  
print("Starting")
i = tap('Ethernet X', phandler)
  
while 7:
    try:
        sleep(0.07)
    except KeyboardInterrupt:
        i.shutdown()
        print("Exiting...")
        break
```

**How it works:**
![](images/ping.png)
> On the screenshot: netspoof.py example script in 1st console, adapter settings, ping in 2nd console

#### Author
[seeklay](https://gitlab.com/seeklay)

#### License: MIT

