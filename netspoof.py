from scapy.all import *
from kmtest import tap
from time import sleep

def phandler(self, pack):
    if ARP in pack:
        if pack[ARP].op != ARP.op.s2i['who-has']:
            return
        response = Ether(src='FC:FC:48:AE:AE:AE', dst=pack[Ether].src)/\
            ARP(
                op = ARP.op.s2i['is-at'],
                hwsrc = 'FC:FC:48:AE:AE:AE',
                psrc = pack[ARP].pdst,
                hwdst = pack[Ether].src,
                pdst = pack[ARP].psrc
            )
        self.sendp(response)
        print(F"Respond to ARP who-has to addr {pack[ARP].pdst} as FC:FC:48:AE:AE:AE")
        return
  
    if ICMP in pack:
        if pack[ICMP].type != ICMP.type.s2i['echo-request']:
            return
        response = Ether(src='FC:FC:48:ae:ae:ae', dst=pack[Ether].src)/\
        IP(src=pack[IP].dst, dst=pack[IP].src)/\
        ICMP(type=ICMP.type.s2i['echo-reply'], id=pack[ICMP].id, seq=pack[ICMP].seq)/pack.load
  
        self.sendp(response)
        print(F"Respond to ICMP ping to addr {pack[IP].dst}")
        return
  
print("Starting")
i = tap('Ethernet X', phandler)
  
while 7:
    try:
        sleep(0.07)
    except KeyboardInterrupt:
        i.shutdown()
        print("Exiting...")
        break
