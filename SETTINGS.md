## How to configure your new virtual KM-TEST network interface

#### Step-by-step instruction

##### Open Windows Control Panel
* Open "Start menu" and type "Control Panel"
* Open it

#### Click "Network and Internet"
![](images/nin.png)

##### Click "Network and Sharing Center"
![](images/nasc.png)

##### Click "Change adapter settings"
![](images/cas.png)
##### Done! Here you can rename interface and configure it like you want.
![](images/rst.png)
