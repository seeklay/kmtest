from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='kmtest',
    version='0.1.0',    
    py_modules=['kmtest'],
    description='Python module providing network TAP interface on Windows KM-TEST loopback adapter using pcap driver via scapy.',        
    long_description_content_type="text/markdown",
    long_description=open('README.md', 'rb').read().decode(),    
    install_requires=['scapy'],
    license='MIT',
    url="https://gitlab.com/seeklay/kmtest",
    author='seeklay',
    author_email='rudeboy@seeklay.icu'
)