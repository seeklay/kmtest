## How to create virtual KM-TEST network interface
*You can create as many interfaces as you want*

#### Step-by-step instruction

##### Open Windows Device Manager
* Open "Start menu" and type "Device Manager"
* Open it

##### Сlick  your computer name
![](images/computer_name.png)
##### press "Action" button
![](images/action.png)

##### Click "Add legacy hardware"
![](images/add.png)
##### Click "Next"
![](images/next0.png)
##### Select "Install the hardware that i manyally select from a list (Advanced)"
![](images/next.png)

#### Select "Network adapters"
![](images/net.png)

##### Select "Microsoft" and search for "KM-TEST Loopback Adapter"
![](images/select.png)

##### Click "Next" and wait
![](images/next1.png)

##### Click "Finish"
![](images/finish.png)
##### Done!

#### If you want to uninstall it, do this:

**Select your adapter and right click, then select and click "Uninstall device"**
![](images/uninstall.png)

## How to change new interface settings see [Changing Settings](SETTINGS.md)
